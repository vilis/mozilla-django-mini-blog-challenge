from django.contrib import admin
from .models import Author, Comment, BlogPost

# Register your models here.
admin.site.register(Comment)
admin.site.register(Author)
admin.site.register(BlogPost)