from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('blogs', views.BlogPostListView.as_view(), name='blogs'),
    path('<int:blog_id>/', views.blog_detail, name='blog-detail'),
    path('bloggers', views.AuthorListView.as_view(), name='bloggers'),
    path('blogger/<int:author_id>/', views.author_detail, name='blogger'),
    path('<int:blog_id>/create/', views.add_comment, name='create')
    #path('blog/<int:pk>', views.BookDetailView.as_view(), name='book-detail'),
]