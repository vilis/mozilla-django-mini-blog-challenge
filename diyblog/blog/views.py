from django.shortcuts import render, get_object_or_404
from .models import Author, BlogPost, Comment
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import CommentForm
from django.http import HttpResponse


def index(request):
    """View function for home page of site."""

    # Generate counts of some of the main objects
    num_blogposts = BlogPost.objects.all().count()

    # The 'all()' is implied by default.
    num_authors = Author.objects.count()

    context = {
        'num_authors': num_authors,
        'num_blogposts': num_blogposts,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)

class BlogPostListView(generic.ListView):
    model = BlogPost
    # context_object_name = 'book_list'   # your own name for the list as a template variable
    paginate_by = 4
    
class AuthorListView(generic.ListView):
    model = Author
    
def author_detail(request, author_id):
    author = get_object_or_404(Author, pk=author_id)
    return render(request, 'blogger.html', {'author': author})

def blog_detail(request, blog_id):
    blog = BlogPost.objects.get(pk=blog_id)
    return render(request, 'blog-detail.html', {'blog': blog})


@login_required
def add_comment(request, blog_id):
    blog_post = get_object_or_404(BlogPost, pk=blog_id)
    print('blog id ' , blog_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.blog = blog_post
            comment.save()
            return redirect(blog_post.get_absolute_url())
    else:
        form = CommentForm()

    return render(request, 'create-comment.html', {'form': form})