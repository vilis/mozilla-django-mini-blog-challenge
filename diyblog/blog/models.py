from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

# Used in /admin site


class Comment(models.Model):
    """Model representing a blog comment."""
    comment = models.CharField(max_length=200, help_text='Enter a comment', blank=False)
    blog = models.ForeignKey('BlogPost', on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    
    class Meta:
        ordering = ['date']

    def __str__(self):
        return self.comment


class Author(models.Model):
    name = models.CharField(max_length=50, help_text='Enter author name')
    bio = models.TextField(help_text='Enter bio')

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('blogger', args=[str(self.id)])


class BlogPost(models.Model):
    # Fields
    title = models.CharField(max_length=100, help_text='Enter title')
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Author, on_delete=models.SET_NULL, null=True, related_name='blogposts')
    description = models.TextField(help_text='Description')
    
    class Meta:
        ordering = ['date']

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('blog-detail', args=[str(self.id)])
    
